import youtube from '../apis/youtube';
import { setVideos } from './videoActionsCreators';

export function fetchAndSetVideos(term) {
    return async dispatch => {
        const { data } = await youtube.get('/search', {
            params: {
                q: term
            }
        });

        dispatch(setVideos({
            videos: data.items,
            selectedVideo: data.items[0]
        }));
    };
};