import { ACTIONS } from '../reducers/videoReducer';

export function setVideos({videos = [], selectedVideo}) {
    return {
        type: ACTIONS.SET_VIDEOS,
        videos,
        selectedVideo
    };
};

export function setSelectedVideo(selectedVideo ) {
    return {
        type: ACTIONS.SET_SELECTED_VIDEO,
        selectedVideo
    };
};

export function setSearchTerm(term) {
    return {
        type: ACTIONS.SET_SEARCH_TERM,
        term
    };
};