import axios from 'axios';

const KEY = 'AIzaSyDnuqILkSx94ysC3pw9GDymfXeN9aSYO-0';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part: 'snippet',
        maxResults: 5,
        key: KEY
    }
});
