import React from 'react';
import { connect } from 'react-redux';

import { setSelectedVideo } from '../actions/videoActionsCreators';

export function VideoItem({ video, setSelectedVideo }) {
    const { thumbnails, title } = video.snippet;
    return (
        <div
            onClick={() => setSelectedVideo(video)}
            className="video-item item">
            <img
                className="ui image"
                src={thumbnails.medium.url}
                alt={video.snippet.title}>
            </img>
            <div className="content">
                <div className="header">
                    {title}
                </div>
            </div>
        </div>
    );
};

export default connect(
    null, { setSelectedVideo }
)(VideoItem)