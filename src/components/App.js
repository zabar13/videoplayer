import React, { Component } from 'react';
import { connect } from 'react-redux';

import SearchBar from './SearchBar';
import VideoList from './VideoList';
import VideoDetail from './VideoDetail';
import { fetchAndSetVideos } from '../actions/videoActions';

export class App extends Component {
    componentDidMount = () => {
        this.props.fetchAndSetVideos(this.props.term)
    };

    render() {
        return (
            <div className="ui container">
                <SearchBar />
                <div className="ui grid">
                    <div className="ui row">
                        <div className="eleven wide column">
                            <VideoDetail />
                        </div>
                        <div className="five wide column">
                            <VideoList
                                videos={this.props.videos} />
                        </div>
                    </div>
                </div>
            </div>
        );
    };
};

export default connect(
    state => ({ ...state.videoReducer }),
    { fetchAndSetVideos })(App)