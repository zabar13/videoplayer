import React, { Component } from 'react';
import { connect } from 'react-redux';

import { setSearchTerm } from '../actions/videoActionsCreators';
import { fetchAndSetVideos } from '../actions/videoActions';

export class SearchBar extends Component {
    onInputChange = (e) => {
        this.props.setSearchTerm(e.target.value);
    };

    onFormSubmit = (e) => {
        e.preventDefault();
        this.props.fetchAndSetVideos(this.props.term);
    };

    render() {
        return (
            <div className="ui segment search-bar">
                <form
                    onSubmit={this.onFormSubmit}
                    className="ui form">
                    <div className="field">
                        <label>Video Search</label>
                        <input
                            type="text"
                            // defaultValue={this.props.term}
                            onChange={this.onInputChange}>
                        </input>
                    </div>
                </form>
            </div>
        );
    };
};

export default connect(
    state => ({ term: state.videoReducer.term }),
    { setSearchTerm, fetchAndSetVideos })(SearchBar)