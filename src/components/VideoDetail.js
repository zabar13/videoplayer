import React from 'react';
import { connect } from 'react-redux';

const YOUTUBE_URL = 'https://www.youtube.com/embed/'

export function VideoDetail({ video }) {
    if (!video) {
        return <div>LOADING...</div>
    }

    const videoSrc = `${YOUTUBE_URL}${video.id.videoId}`;

    const { title, description } = video.snippet;
    return (
        <div>
            <div className="ui embed">
                <iframe src={videoSrc} title={title} />
            </div>
            <div className="ui segment">
                <h4 className="ui header">
                    {title}
                </h4>
                <p>{description}</p>
            </div >
        </div>
    );
};

export default connect(
    state => ({ video: state.videoReducer.selectedVideo })
)(VideoDetail)