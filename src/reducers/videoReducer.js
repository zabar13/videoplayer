export const ACTIONS = {
    SET_VIDEOS: Symbol('SET_VIDEOS'),
    SET_SELECTED_VIDEO: Symbol('SET_SELECTED_VIDEO'),
    SET_SEARCH_TERM: Symbol('SET_SEARCH_TERM')
};

const DEFAULT_STATE = {
    videos: [],
    selectedVideo: null,
    term: 'plane'
}

export default function videoReducer(state = DEFAULT_STATE,
    { type, videos, selectedVideo, term }) {
    switch (type) {
        case ACTIONS.SET_SEARCH_TERM:
            return {
                ...state,
                term
            };
        case ACTIONS.SET_VIDEOS:
            return {
                ...state,
                videos: [...videos],
                selectedVideo

            };
        case ACTIONS.SET_SELECTED_VIDEO:
            return {
                ...state,
                selectedVideo
            };
        default:
            return state
    };
}