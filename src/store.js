import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import videoReducer from './reducers/videoReducer'

const store = createStore(combineReducers({
    videoReducer
}),applyMiddleware(thunk));

export default store;